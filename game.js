var canvas = document.querySelector("canvas");
var ctx = canvas.getContext("2d");
var routeMap;
var robot;
var human;
var previousTime = Date.now();
var currentTime = Date.now();
var speed = 0.1;

//creates scene objects and start drawing
function onLoad(e) {
    navigator.geolocation.getCurrentPosition(gpsOk, gpsError);
//    document.body.focus();       
    routeMap = new RouteMap();
    robot = new Robot(30, 30, 0);
    human = new Human(30, 30, 11);
    draw();
}
window.addEventListener("load", onLoad);

var gpsError = function(e) {
    document.body.addEventListener('keypress', keyPressed);
    canvas.addEventListener('mousemove', mouseMoved, false);
    mousePosition = "";
    alert("You ain't lucky. GPS doesn't work\n"+e.message);  
};

var gpsOk = function(position) {
    document.body.addEventListener('keypress', keyPressed);
    canvas.addEventListener('mousemove', mouseMoved, false);
    mousePosition = "";
    alert("You might be sitting at coordinates ("+[position.coords.latitude, position.coords.longitude]+ ") on pathetic planet named Earth. But right now you have ran away (or hunt).");
};

//Enumeration of direction
//Very useful in future extansion with more then two movable objects
var DirectionEnum = {
    left: 1,
    up: 2,
    right: 3,
    down: 4,
    properties: {
        1: {name: "left", value: 1, code: "L"},
        2: {name: "up", value: 2, code: "U"},
        3: {name: "right", value: 3, code: "R"},
        4: {name: "down", value: 4, code: "D"}
    }
};

//dumm function processing keystrokes
function keyPressed(e) {
    var c = e.keyCode || e.which;
    console.log(c);
    switch (c) {
        case 37:    //left
            robot.futureDirection = DirectionEnum.left;
            break;
        case 38:    //up
            robot.futureDirection = DirectionEnum.up;
            break;
        case 39:    //right
            robot.futureDirection = DirectionEnum.right;
            break;
        case 40:    //down
            robot.futureDirection = DirectionEnum.down;
            break;

        case 97:    //left
            human.futureDirection = DirectionEnum.left;
            break;
        case 119:    //up
            human.futureDirection = DirectionEnum.up;
            break;
        case 100:    //right
            human.futureDirection = DirectionEnum.right;
            break;
        case 115:    //down
            human.futureDirection = DirectionEnum.down;
            break;
    }
    if (human.direction === undefined && human.futureDirection !== undefined) {
        human.direction = human.futureDirection;
    }
    if (robot.direction === undefined && robot.futureDirection !== undefined) {
        robot.direction = robot.futureDirection;
    }

    //prevent scrolling by arrows
    if (c >= 37 && c <= 40) {
        e.preventDefault();
    }
}

//Base "class" of Drawable objects. Movable and Nodes inherts from it.
//Every drawable object is shifted by 0.5 in x and y due to sharper result
//width and height use only even values 
var Drawable = function(centerX, centerY, width, height) {
    this.centerX = centerX + 0.5;
    this.centerY = centerY + 0.5;
    this.x = this.centerX - width / 2;
    this.y = this.centerY - height / 2;
    this.width = width;
    this.height = height;
};

//When setting center position x and y (left upper corner) must be update too.
Drawable.prototype.setCenterX = function(centerX) {
    if (this.centerX !== centerX) {
        this.centerX = centerX + 0.5;
        this.x = this.centerX - this.width / 2;
    }
};

//When setting center position x and y (left upper corner) must be update too.
Drawable.prototype.setCenterY = function(centerY) {
    if (this.centerY !== centerY) {
        this.centerY = centerY + 0.5;
        this.y = this.centerY - this.height / 2;
    }
};

//Base "class" for all objects that moves
var Movable = function(width, height, starUpNodeId) {
    Drawable.call(this, routeMap.nodes[starUpNodeId].centerX, routeMap.nodes[starUpNodeId].centerY, width, height);
    this.direction;
    this.futureDirection;
    this.destinationNodeId = starUpNodeId;
};
Movable.prototype = Object.create(Drawable.prototype);
Movable.prototype.constructor = Movable;

//Returns true or false according to position of movable object.
//Function is used to decide when to change directions.
Movable.prototype.isAtDestinationNode = function() {
    switch (this.direction) {
        case DirectionEnum.up:
            if (this.centerY <= routeMap.nodes[this.destinationNodeId].centerY) {
                return true;
            }
            break;
        case DirectionEnum.down:
            if (this.centerY >= routeMap.nodes[this.destinationNodeId].centerY) {
                return true;
            }
            break;
        case DirectionEnum.left:
            if (this.centerX <= routeMap.nodes[this.destinationNodeId].centerX) {
                return true;
            }
            break;
        case DirectionEnum.right:
            if (this.centerX >= routeMap.nodes[this.destinationNodeId].centerX) {
                return true;
            }
            break;
    }
    return false;
};

//updates posistion of movable objects. Handles directions changes at nodes.
Movable.prototype.updatePosition = function() {
    if (this.isAtDestinationNode()) {
        //set center to destination coordinates in case we moved over it
        this.setCenterX(routeMap.nodes[this.destinationNodeId].centerX);
        this.setCenterY(routeMap.nodes[this.destinationNodeId].centerY);
        this.direction = this.futureDirection;
        //retrive next destinationNode from direction and neighbors of current destinationNode
        var neighbors = routeMap.nodes[this.destinationNodeId].neighbors;
        var directionString = DirectionEnum.properties[this.futureDirection].name;
        this.destinationNodeId = neighbors[directionString];
        console.log("dir " + this.direction);
        console.log("fdir " + this.futureDirection);
        console.log("dest " + this.destinationNodeId);
    }
    else {
        //just the movement
        var distance = (currentTime - previousTime) * speed;
        switch (this.direction) {
            case DirectionEnum.up:
                this.setCenterY(this.centerY - distance);
                break;
            case DirectionEnum.down:
                this.setCenterY(this.centerY + distance);
                break;
            case DirectionEnum.left:
                this.setCenterX(this.centerX - distance);
                break;
            case DirectionEnum.right:
                this.setCenterX(this.centerX + distance);
                break;
        }
    }
};

//Class of one type of movable object
var Robot = function(width, height, startUpNode) {
    Movable.call(this, width, height, startUpNode);
    this.color = "grey";
};
Robot.prototype = Object.create(Movable.prototype);
Robot.prototype.constructor = Robot;

Robot.prototype.draw = function() {
    this.updatePosition();
    ctx.beginPath();
    ctx.rect(this.x, this.y, this.width, this.height);
    ctx.fillStyle = this.color;
    ctx.stroke();
    ctx.fill();
};

//Class of one type of movable object
var Human = function(width, height, startUpNode) {
    Movable.call(this, width, height, startUpNode);
    this.color = "pink";
};
Human.prototype = Object.create(Movable.prototype);
Human.prototype.constructor = Human;

Human.prototype.draw = function() {
    this.updatePosition();
    ctx.beginPath();
    ctx.rect(this.x, this.y, this.width, this.height);
    ctx.fillStyle = this.color;
    ctx.stroke();
    ctx.fill();
};

//Main drawing function
var draw = function() {
    currentTime = Date.now();

    //draw scene
    ctx.clearRect(0, 0, 800, 600);      //clear whole canvas
    routeMap.draw();
    robot.draw();
    human.draw();
    writeMessage(canvas, mousePosition);      //write mouse position in canvas

    previousTime = currentTime;
    requestAnimationFrame(draw);
};

//Route map with hardcoded nodes and edges
//There is preparation for map editor. See addNode() and getMousePosition() functions
var RouteMap = function() {
    this.nodeSize = 6;      //node is square
    this.nodes = [
        new Node(0, 70, 70, this.nodeSize, {down: 9, right: 1}),
        new Node(1, 450, 70, this.nodeSize, {down: 4, left: 0, right: 2}),
        new Node(2, 700, 70, this.nodeSize, {down: 11, left: 1}),
        new Node(3, 300, 200, this.nodeSize, {down: 6, right: 4}),
        new Node(4, 450, 200, this.nodeSize, {up: 1, left: 3, right: 5}),
        new Node(5, 600, 200, this.nodeSize, {left: 4, down: 8}),
        new Node(6, 300, 400, this.nodeSize, {up: 3, right: 7}),
        new Node(7, 450, 400, this.nodeSize, {left: 6, right: 8, down: 10}),
        new Node(8, 600, 400, this.nodeSize, {left: 7, up: 5}),
        new Node(9, 70, 550, this.nodeSize, {up: 0, right: 10}),
        new Node(10, 450, 550, this.nodeSize, {up: 7, left: 9, right: 11}),
        new Node(11, 700, 550, this.nodeSize, {up: 2, left: 10})
    ];
    this.edges = [
        new Edge(0, 0, 1),
        new Edge(1, 1, 2),
        new Edge(2, 1, 4),
        new Edge(3, 0, 9),
        new Edge(4, 3, 4),
        new Edge(5, 4, 5),
        new Edge(6, 2, 11),
        new Edge(7, 3, 6),
        new Edge(8, 5, 8),
        new Edge(9, 6, 7),
        new Edge(10, 7, 8),
        new Edge(11, 9, 10),
        new Edge(12, 7, 10),
        new Edge(13, 10, 11)
    ];
    this.color = "black";
};

RouteMap.prototype.draw = function() {
    ctx.beginPath();
    for (i = 0; i < this.nodes.length; i++) {
        ctx.rect(this.nodes[i].x, this.nodes[i].y, this.nodeSize, this.nodeSize);
    }
    for (i = 0; i < this.edges.length; i++) {
        ctx.moveTo(this.nodes[this.edges[i].from].x + this.nodeSize / 2, this.nodes[this.edges[i].from].y + this.nodeSize / 2);
        ctx.lineTo(this.nodes[this.edges[i].to].x + this.nodeSize / 2, this.nodes[this.edges[i].to].y + this.nodeSize / 2);
    }
    ctx.fillStyle = this.color;
    ctx.stroke();
    ctx.fill();
};

//"class" of node. It inherits form Drawable
var Node = function(id, centerX, centerY, size, neighbors) {
    Drawable.call(this, centerX, centerY, size, size);
    this.neighbors = neighbors;
    this.id = id;
};
Node.prototype = Object.create(Drawable.prototype);
Node.prototype.constructor = Node;

var Edge = function(id, from, to) {
    this.id = id;
    this.from = from;
    this.to = to;
};

//show position of mouse in canvas and futureDirection.
//good for debugging
function writeMessage(canvas, message) {
    var context = canvas.getContext('2d');
    context.font = '10pt Calibri';
    context.fillStyle = 'black';
    context.fillText(message /*+ " FutureDir: " + robot.futureDirection*/, 10, 25);
}

//Preparation for map editor
function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: Math.round(evt.clientX - rect.left),
        y: Math.round(evt.clientY - rect.top)
    };
}
var mousePosition = "Initiating... Wait for gps response.";
function mouseMoved(evt){
    var mousePos = getMousePos(canvas, evt);
    mousePosition = 'Mouse position: ' + mousePos.x + ',' + mousePos.y;
}
//canvas.addEventListener('click', addNode);

function addNode(evt) {
    var mousePos = getMousePos(canvas, evt);
    routeMap.push(new Node(mousePos.x, mousePos.y));
}


